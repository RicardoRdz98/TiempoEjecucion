package com.example.root.tiempoejecucion;

import android.app.ActivityManager;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lista;
    private ArrayAdapter<String> adapter = null;
    private ArrayList<String> eventos = new ArrayList<>();


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] arreglo = {"facebook", "chrome", "telegram"};
        
        lista = (ListView) findViewById(R.id.List);

        ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        long currentMillis = Calendar.getInstance().getTimeInMillis();
        Calendar cal = Calendar.getInstance();

        for (ActivityManager.RunningServiceInfo info : services) {
            cal.setTimeInMillis(currentMillis-info.activeSince);

            //Log.i("Error: ", String.format("Process %s has been running since: %d ms",info.process,  info.activeSince));

            int res = milisegundos2tiempo(Integer.parseInt(String.valueOf(info.activeSince)));
            Date fecha1;
            fecha1 = new Date(100,0,0,res%10000000,res%100000,res%1000);

            String tiempo = fecha1.getHours()+" horas: "+fecha1.getMinutes()+" minutos ";

            for (int i = 0; i < arreglo.length; i++) {
                int intIndex = info.process.indexOf(arreglo[i]);
                if(intIndex != -1){
                    eventos.add(String.format(info.process + " " + tiempo));
                }
            }

        }

        adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, eventos);
        lista.setAdapter(adapter);

    }

    public static int milisegundos2tiempo(int ms)
    {
        int mili = ms%1000; ms -= mili; ms /= 1000;
        int segs = ms%60; ms -= segs; ms /= 60;
        int mins = ms%60; ms -= mins; ms /= 60;
        int horas = ms;
        return horas*1000*100*100 + mins*1000*100 + segs*1000 + mili;
    }

}
